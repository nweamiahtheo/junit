package JUnint.Testcase;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCase {
    @Test
    public void add_twoPlusTwo_returnsFour(){
        final int expected = 4;
        final int actual = Math.addExact(2, 2);
        assertEquals("2+2 is 4", actual, expected);
    }
}






